#include "demo.h"
#include "ui_demo.h"

#include "QTechUI/style.h"

Demo::Demo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Demo)
{
    ui->setupUi(this);
    QTechUI::LoadTheme("dark");
}

Demo::~Demo()
{
    delete ui;
}