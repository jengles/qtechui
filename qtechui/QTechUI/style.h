#pragma once

#include "qtechui_export.h"

#include <QString>

class QTECHUI_EXPORT QTechUI {

public:
    static void LoadTheme(const QString& theme);
};