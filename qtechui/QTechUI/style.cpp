#include "style.h"

#include <QApplication>
#include <QFile>
#include <QDebug>
#include <QFile>
#include <QFontDatabase>
#include <QResource>

void QTechUI::LoadTheme(const QString& theme) {
	// Load base resources
	QString baseResourcePath = QString("%1/base.rcc").arg(QApplication::applicationDirPath());
	QResource::registerResource(baseResourcePath);
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-Black.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-BlackItalic.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-Bold.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-BoldItalic.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-Italic.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-Light.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-LightItalic.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-Medium.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-MediumItalic.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-Regular.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-Thin.ttf");
	QFontDatabase::addApplicationFont(":/qtechui/fonts/Roboto-ThinItalic.ttf");


	QString themeResourcePath = QString("%1/themes/%2.rcc").arg(QApplication::applicationDirPath()).arg(theme);
	QResource::registerResource(themeResourcePath);
	QFile styleSheetFile(QString(":/qtechui/%1.qss").arg(theme));
	styleSheetFile.open(QIODevice::ReadOnly);
	qApp->setStyleSheet(styleSheetFile.readAll());
}