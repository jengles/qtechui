import sys
import os

import qtsass

QRC_BASE = """
<!DOCTYPE RCC>
<RCC version="1.0">
<qresource prefix="/qtechui">
    <file>{}</file>
</qresource>
</RCC>
"""


def generate_theme(input_scss, output_dir):
    os.makedirs(output_dir, exist_ok=True)
    output_file_prefix = os.path.splitext(os.path.basename(input_scss))[0]
    output_qss_filename =  output_file_prefix + ".qss"
    output_qss_path = os.path.normpath(os.path.join(output_dir, output_qss_filename))
    output_qrc_path = os.path.normpath(os.path.join(output_dir, output_file_prefix + ".qrc"))

    qtsass.compile_filename(input_scss, output_qss_path)

    qrc_data = QRC_BASE.format(output_qss_filename)
    print("writing: {}".format(output_qrc_path))
    with open(output_qrc_path, "w") as f:
        f.write(qrc_data)


if __name__ == "__main__":
    generate_theme(os.path.normpath(sys.argv[1]), os.path.normpath(sys.argv[2]))
