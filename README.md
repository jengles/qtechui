# QTechUI

QTechUI is a lightweight collection of utilities to build clean, consistent, and beautiful Qt interfaces with a focus on dense technical UI for custom software and plugins.