find_package(Qt5Core REQUIRED)

get_target_property(_qmake_executable Qt5::qmake IMPORTED_LOCATION)
get_filename_component(_qt_bin_dir "${_qmake_executable}" DIRECTORY)

function(windeployqt target)

    add_custom_command(TARGET ${target} POST_BUILD
        COMMAND "${_qt_bin_dir}/windeployqt.exe"
            --verbose 1
            \"$<TARGET_FILE:${target}>\"
        COMMENT "Deploying Qt libraries using windeployqt.exe for '${target}' ..."
    )

endfunction()